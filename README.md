# lyceum-animations

Animations du site lyceum.fr accessibles à l'adresse:

http://lyceum.fr/animations

## Animations disponibles

### [effet-doppler](http://lyceum.fr/animations/effet-doppler)

Il s'agit d'un fork d'une des pages de *Chrome web audio examples*: [visalizer-gl](https://github.com/GoogleChrome/web-audio-samples/blob/gh-pages/samples/audio/doppler.html)(Licence MIT Copyright 2014 The Chromium Authors. All rights reserved.)

### [realtime-analyzer](http://lyceum.fr/animations/realtime-visualizer)

Il s'agit d'un fork d'une des pages de *Chrome web audio examples*: [visalizer-gl](https://github.com/GoogleChrome/web-audio-samples/blob/gh-pages/samples/audio/visualizer-gl.html)(Licence MIT Copyright 2014 The Chromium Authors. All rights reserved.)

La bibliothèque de sons provient de [midi-js-soundfonts](https://github.com/gleitz/midi-js-soundfonts), elle est incluse sous la forme d'un sous-module.(Licence MIT Copyright (C) 2012 Benjamin Gleitzman (gleitz@mit.edu))

### [formule-toplogique](http://lyceum.fr/animations/formule-topologique)

Cette animation permet d'explorer les liens entre formule topologique, modèle moléculaire et nom d'une molécule.

### [rmn-predict](http://lyceum.fr/animations/rmn-predict)

Cette animation permet de prévoir un spectre RMN à partir d'une formule toplogique.

Source: [Démonstration originale sur le site de jmol](http://chemapps.stolaf.edu/jmol/jsmol/jsv_predict.htm)

### [carbone-asymetrique(http://lyceum.fr/animations/carbone-asymetrique)

Démonstration de l'existence de deux molécules chirales différentes à partir de l'exemple de CHIBrCl.

Les modèles ont été créés grâce à openbabel:

    obabel -:"[C@@](Cl)([H])(I)Br" --gen3D -O R-CHIBrClH.svg
    obabel -:"[C@@](Cl)([H])(I)Br" --gen3D -O R-CHIBrClH.mol
    obabel -:"[C@@](Br)([H])(I)Cl" --gen3D -O S-CHIBrClH.mol
    obabel -:"[C@@](Br)([H])(I)Cl" --gen3D -O S-CHIBrClH.svg

**Crédits**

Le visualisateur 3D est [jsmol](http://sourceforge.net/projects/jmol/files/)(Licence LGPL), il est intégré sous la forme d'un sous-module à partir du répertoire [jsmol-dist](https://github.com/benjaminabel/jsmol-dist).

L'éditeur 2D utilisé est [jsme](http://www.molinspiration.com/jme/index.html)(Copyright (c) 2013, Novartis Institutes for BioMedical Research Inc and Bruno Bienfait)

La recherche de structure se fait par l'intermédiaire de la base de données [NCI](http://cactus.nci.nih.gov/ncidb2.2/)

- Modélisation moléculaire: Cette page permet d'observer le lien existant entre la formule topologique d'une molécule et son modèle moléculaire

**Todo**

- design: Les écrans du lycée ont une résolution de 1024x768, le navigateur a alors une résolution de 1007x629 en mode fenêtre et 1007x767 en mode plein écran.

- https: Problème dans la requête vers la base NCI si passage par https.

## Développement

Pour ajouter une animation, ajouter un dossier dans le répertoire `src`, et un lien dans la page d'accueil des animations `src/index.html`.

Créer un fichier de règles de filtrage pour rsync (voir filter rules dans le manpage de rsync) nommé `.rsync-filter` à la racine du répertoire de l'animation qui indique quels fichiers exclure lors de la création du répertoire `dist`.

Pour publier vers le serveur du site `lyceum.fr`, lancer `make publish`

### Gestion des dépendences

Si une dépendence n'est utilisée que dans une animation, elle est incluse dans le répértoire de l'animation, sinon elle est incluse à la racine du répertoire `src` dans un dossier nommé `src/vendor`.

Pour mettre à jour toutes les dépendences mises sous formes de submodules:

  git submodule foreach git pull origin master
